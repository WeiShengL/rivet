Name: CLEOII_1996_I397787
Year: 1996
Summary: Mass distributions in $\Xi_c^+\to\Sigma^+K^-\pi^+$
Experiment: CLEOII
Collider: CESR
InspireID: 397787
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 365 (1996) 431-436
RunInfo: Any process producing Xic+ mesons
Description:
  'Measurement of the mass distributions in the decay $\Xi_c^+\to\Sigma^+K^-\pi^+$ by CLEOII. The data were read from the plots in the paper and may not have been corrected for efficiency/acceptance.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:1995gpy
BibTeX: '@article{CLEO:1995gpy,
    author = "Bergfeld, T. and others",
    collaboration = "CLEO",
    title = "{Observation of the Xi(c)+ charmed baryon decays to Sigma+ K- pi+, Sigma+ anti-K*0, and Lambda K- pi+ pi+}",
    eprint = "hep-ex/9508006",
    archivePrefix = "arXiv",
    reportNumber = "CLNS-95-1349, CLEO-95-12",
    doi = "10.1016/0370-2693(95)01432-2",
    journal = "Phys. Lett. B",
    volume = "365",
    pages = "431--436",
    year = "1996"
}
'
