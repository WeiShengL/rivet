BEGIN PLOT /CLEOC_2011_I913909/d01-x01-y01
Title=$K^0_S\pi^0$ mass distribution in $D^0\to K_S^0\pi^0\pi^0$
XLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOC_2011_I913909/d01-x01-y02
Title=$\pi^0\pi^0$ mass distribution in $D^0\to K_S^0\pi^0\pi^0$
XLabel=$m^2_{\pi^0\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOC_2011_I913909/dalitz
Title=Dalitz plot for  $D^0\to K_S^0\pi^0\pi^0$
XLabel=$m^2_{\pi^0\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K_S^0\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^0\pi^0}/{\rm d}m^2_{K_S^0\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
