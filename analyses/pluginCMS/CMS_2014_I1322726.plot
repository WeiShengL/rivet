BEGIN PLOT /CMS_2014_I1322726/d01-x01-y01
Title=CMS, 2.76 TeV, dimuon
XLabel=$p_T$ [GeV]
YLabel=$d^2 \sigma / dy dp_T$ (pb)
END PLOT 

BEGIN PLOT /CMS_2014_I1322726/d02-x01-y01
Title=CMS, 2.76 TeV, dielectron
XLabel=$p_T$ [GeV]
YLabel=$d^2 \sigma / dy dp_T$ (pb)
END PLOT 

BEGIN PLOT /CMS_2014_I1322726/d03-x01-y01
Title=CMS, 2.76 TeV, dimuon
XLabel=$|y|$
YLabel=$d\sigma/dy$ (pb)
LogY=0
END PLOT 

BEGIN PLOT /CMS_2014_I1322726/d04-x01-y01
Title=CMS, 2.76 TeV, dielectron
XLabel=$|y|$ 
YLabel=$d\sigma/dy$ (pb)
LogY=0
END PLOT 
