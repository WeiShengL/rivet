BEGIN PLOT /ALICE_2021_I1862793/d01-x01-y01
Title=Cross section of prompt $\Xi_\mathrm{c}^{0}$ at $\sqrt{s} = 13$ TeV for $|y| < 0.5$
XLabel=$\mathrm{p}_\mathrm{T}^{\Xi^{0}_\mathrm{c}}$ [GeV/c]
YLabel=$\frac{\mathrm{d}^{2}\sigma}{\mathrm{dp}_{\mathrm{T}} \mathrm{d}y}$ [$\mu$b/(GeV/c)]
YLabelSep=5
LegendXPos=0.56
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1862793/d02-x01-y01
Title=Cross section of prompt $\Xi_\mathrm{c}^{+}$ at $\sqrt{s} = 13$ TeV for $|y| < 0.5$
XLabel=$\mathrm{p}_\mathrm{T}^{\Xi^{+}_\mathrm{c}}$ [GeV/c]
YLabel=$\frac{\mathrm{d}^{2}\sigma}{\mathrm{dp}_{\mathrm{T}} \mathrm{d}y}$ [$\mu$b/(GeV/c)]
YLabelSep=5
LegendXPos=0.56
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1862793/d03-x01-y01
Title=$\Xi_\mathrm{c}^{0}/\mathrm{D^{0}}$ ratio at $\sqrt{s} = 13$ TeV for $|y| < 0.5$
XLabel=$\mathrm{p}_\mathrm{T}$ [GeV/c]
YLabel=$\frac{\sigma (\Xi_\mathrm{c}^{0})}{\sigma (\mathrm{D}^{0})}$
YLabelSep=5
LegendXPos=0.56
LegendYPos=0.94
YMax=2
END PLOT

BEGIN PLOT /ALICE_2021_I1862793/d04-x01-y01
Title=$\Xi_\mathrm{c}^{+}/\mathrm{D^{0}}$ ratio at $\sqrt{s} = 13$ TeV for $|y| < 0.5$
XLabel=$\mathrm{p}_\mathrm{T}$ [GeV/c]
YLabel=$\frac{\sigma (\Xi_\mathrm{c}^{+})}{\sigma (\mathrm{D}^{0})}$
YLabelSep=5
LegendXPos=0.56
LegendYPos=0.94
YMax=2
END PLOT

BEGIN PLOT /ALICE_2021_I1862793/d05-x01-y01
Title=$\Xi_\mathrm{c}^{0}/{\Lambda_\mathrm{c}^{+}}$ ratio at $\sqrt{s} = 13$ TeV for $|y| < 0.5$
XLabel=$\mathrm{p}_\mathrm{T}$ [GeV/c]
YLabel=$\frac{\sigma (\Xi_\mathrm{c}^{0})}{\sigma (\Lambda_\mathrm{c}^{+})}$
YLabelSep=5
LegendXPos=0.56
LegendYPos=0.92
YMax=5
END PLOT

BEGIN PLOT /ALICE_2021_I1862793/d06-x01-y01
Title=$\Xi_\mathrm{c}^{0, +}/{\Sigma_\mathrm{c}^{0,+,++}}$ ratio at $\sqrt{s} = 13$ TeV for $|y| < 0.5$
XLabel=$\mathrm{p}_\mathrm{T}$ [GeV/c]
YLabel=$\frac{\sigma (\Xi_\mathrm{c}^{0,+})}{\sigma (\Sigma_\mathrm{c}^{0,+,++})}$
YLabelSep=5.7
LegendXPos=0.56
LegendYPos=0.94
YMax=20
END PLOT

BEGIN PLOT /ALICE_2021_I1862793/d07-x01-y01
Title=$p_\mathrm{T}-$integrated cross section of $\Xi_\mathrm{c}^{0}$ for $ 1 < \mathrm{p}_\mathrm{T} < 12$
XLabel=Integrated $\mathrm{p}_\mathrm{T}$ for $1<\mathrm{p}_\mathrm{T}<12$ [GeV/c]
YLabel= $\Xi_\mathrm{c}^{0}$     $\frac{\mathrm{d}\sigma}{\mathrm{d}y}$ [$\mu$b]
YLabelSep=5
LegendXPos=0.56
LegendYPos=0.80
END PLOT

BEGIN PLOT /ALICE_2021_I1862793/d08-x01-y01
Title=$p_\mathrm{T}-$integrated cross section of $\Xi_\mathrm{c}^{0}$ for full $\mathrm{p}_\mathrm{T}$ range
XLabel=Full integrated $\mathrm{p}_\mathrm{T}$ [GeV/c]
YLabel= ${\Xi_\mathrm{c}^{0}}$     $\frac{\mathrm{d}\sigma}{\mathrm{d}y}$ [$\mu$b]
YLabelSep=5
LegendXPos=0.56
LegendYPos=0.80
END PLOT

BEGIN PLOT /ALICE_2021_I1862793/d09-x01-y01
Title=$p_\mathrm{T}-$integrated cross section of $\Xi_\mathrm{c}^{+}$ at $\sqrt{s} = 13$ TeV, $|y|<0.5$
XLabel=Integrated $\mathrm{p}_\mathrm{T}$ for $4<\mathrm{p}_\mathrm{T}<12$ [GeV/c]
YLabel= ${\Xi_\mathrm{c}^{+}}$     $\frac{\mathrm{d}\sigma}{\mathrm{d}y}$ [$\mu$b]
YLabelSep=5
LegendXPos=0.56
LegendYPos=0.80
END PLOT
