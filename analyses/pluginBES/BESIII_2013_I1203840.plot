BEGIN PLOT /BESIII_2013_I1203840/d01-x01-y01
Title=$\bar{p}\Sigma^0$ mass distribution in $\psi(2S)\to \bar{p}\Sigma^0K^+$+c.c.
XLabel=$m_{\bar{p}\Sigma^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\Sigma^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1203840/d01-x01-y02
Title=$\Sigma^0K^+$ mass distribution in $\psi(2S)\to \bar{p}\Sigma^0K^+$+c.c.
XLabel=$m_{\Sigma^0K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^0K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2013_I1203840/d02-x0[1,2,3]-y01
XLabel=$m_{\bar{p}K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1203840/d02-x0[1,2,3]-y02
XLabel=$m_{\Lambda^0K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda^0K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2013_I1203840/d02-x01-y01
Title=$m_{\bar{p}K^+}$ mass distribution in $\chi_{c0}\to\bar{p}K^+\Lambda^0$
END PLOT
BEGIN PLOT /BESIII_2013_I1203840/d02-x02-y01
Title=$m_{\bar{p}K^+}$ mass distribution in $\chi_{c1}\to\bar{p}K^+\Lambda^0$
END PLOT
BEGIN PLOT /BESIII_2013_I1203840/d02-x03-y01
Title=$m_{\bar{p}K^+}$ mass distribution in $\chi_{c2}\to\bar{p}K^+\Lambda^0$
END PLOT

BEGIN PLOT /BESIII_2013_I1203840/d02-x01-y02
Title=$m_{\Lambda^0K^+}$ mass distribution in $\chi_{c0}\to\bar{p}K^+\Lambda^0$
END PLOT
BEGIN PLOT /BESIII_2013_I1203840/d02-x02-y02
Title=$m_{\Lambda^0K^+}$ mass distribution in $\chi_{c1}\to\bar{p}K^+\Lambda^0$
END PLOT
BEGIN PLOT /BESIII_2013_I1203840/d02-x03-y02
Title=$m_{\Lambda^0K^+}$ mass distribution in $\chi_{c2}\to\bar{p}K^+\Lambda^0$
END PLOT

BEGIN PLOT /BESIII_2013_I1203840/d03-x01-y01
Title=$m_{\bar{p}\Lambda^0}$ mass distribution in $\chi_{c0}\to\bar{p}K^+\Lambda^0$
XLabel=$m_{\bar{p}\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\Lambda^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
