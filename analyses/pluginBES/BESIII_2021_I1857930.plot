BEGIN PLOT /BESIII_2021_I1857930/d01-x01-y01
Title=$\sigma(e^+e^-\to \phi\eta)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \phi\eta)$/pb
ConnectGaps=1
#LogY=0
END PLOT
