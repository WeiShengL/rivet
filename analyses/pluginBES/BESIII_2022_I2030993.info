Name: BESIII_2022_I2030993
Year: 2022
Summary: Dalitz decay of $D_s^+\to\pi^+\pi^0\eta^\prime$
Experiment: BESIII
Collider: BEPC
InspireID: 2030993
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2202.04232 [hep-ex]
RunInfo: Any process producing D_s+ mesons
Description:
  'Measurement of the mass distributions in the decay $D^+_s\to \pi^+\pi^0\eta^\prime$ by BES. The data were read from the plots in the paper. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events using model in the paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022ewq
BibTeX: '@article{BESIII:2022ewq,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Amplitude analysis and branching-fraction measurement of $D_{s}^{+} \to \pi^{+}\pi^{0}\eta^{\prime}$}",
    eprint = "2202.04232",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "2",
    year = "2022"
}
'
