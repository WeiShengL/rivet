BEGIN PLOT /BESIII_2021_I1997451/d01-x01-y01
Title=$\sigma(e^+e^-\to \phi\pi^+\pi^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \phi\pi^+\pi^-)$/pb
ConnectGaps=1
END PLOT
