BEGIN PLOT /BESIII_2020_I1799437/d01-x01-y01
Title=$K^+K^-$ mass distribution in $D^0\to K^0_SK^+K^-$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1799437/d01-x01-y02
Title=$K^0_SK^+$ mass distribution in $D^0\to K^0_SK^+K^-$
XLabel=$m^2_{K^0_SK^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1799437/d01-x01-y03
Title=$K^0_SK^-$ mass distribution in $D^0\to K^0_SK^+K^-$
XLabel=$m^2_{K^0_SK^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1799437/dalitz
Title=Dalitz plot for $D^0\to K^0_SK^+K^-$
YLabel=$m^2_{K^0_SK^+}$ [$\mathrm{GeV}^{2}$]
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_SK^+}/{\rm d}m^2_{K^+K^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
