BEGIN PLOT /BESIII_2019_I1716256/d01-x01-y01
Title=Differential $\chi_{c1}\to J/\psi\mu^+\mu^-$ decay
XLabel=$m_{\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\text{Br}/\text{d}q$ [$\text{GeV}^{-1}$]
LegendXPos=0.1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1716256/d01-x01-y02
Title=Form Factor for $\chi_{c1}\to J/\psi\mu^+\mu^-$ decay
XLabel=$m_{\mu^+\mu^-}$ [GeV]
YLabel=$\left|F\left(m^2_{\mu^+\mu^-}\right)\right|^2$
LegendXPos=0.1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1716256/d02-x01-y01
Title=Differential $\chi_{c2}\to J/\psi\mu^+\mu^-$ decay
XLabel=$m_{\mu^+\mu^-}$ [GeV]
YLabel=$\text{d}\text{Br}/\text{d}q$ [$\text{GeV}^{-1}$]
LegendXPos=0.1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1716256/d02-x01-y02
Title=Form Factor for $\chi_{c2}\to J/\psi\mu^+\mu^-$ decay
XLabel=$m_{\mu^+\mu^-}$ [GeV]
YLabel=$\left|F\left(m^2_{\mu^+\mu^-}\right)\right|^2$
LegendXPos=0.1
LogY=0
END PLOT
