BEGIN PLOT /BESIII_2017_I1469067/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution for $\eta^\prime\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1469067/d01-x01-y02
Title=$\pi^+\pi^0$ mass distribution for $\eta^\prime\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1469067/d01-x01-y03
Title=$\pi^-\pi^0$ mass distribution for $\eta^\prime\to \pi^+\pi^-\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1469067/d01-x01-y04
Title=$\pi^0\pi^0$ mass distribution for $\eta^\prime\to \pi^0\pi^0\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
