BEGIN PLOT /BESIII_2022_I1945692/d01-x01-y01
Title=$K^0_SK^0_S$ mass distribution in $D_s^+\to K^0_S K^0_S \pi^+$
XLabel=$m_{K^0_SK^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_SK^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I1945692/d01-x01-y02
Title=$K^0_S\pi^+$ mass distribution in $D_s^+\to K^0_S K^0_S \pi^+$
XLabel=$m_{K^0_S\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I1945692/dalitz
Title=Dalitz plot for $D_s^+\to K^0_S K^0_S \pi^+$
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_S\pi^+}/{\rm d}m^2_{K^0_S\pi^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

