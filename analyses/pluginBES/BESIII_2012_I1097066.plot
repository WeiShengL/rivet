BEGIN PLOT /BESIII_2012_I1097066/d01-x01-y01
Title=$\cos\beta$ distribution in $\psi(2S)\to J/\psi\gamma\gamma$
XLabel=$|\cos\beta|$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}|\cos\beta|$
END PLOT
BEGIN PLOT /BESIII_2012_I1097066/d01-x01-y02
Title=$\cos\theta_\ell$ distribution in $\psi(2S)\to J/\psi\gamma\gamma$
XLabel=$|\cos\theta_\ell|$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}|\cos\theta_\ell|$
END PLOT
