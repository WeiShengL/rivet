BEGIN PLOT /BABAR_2012_I1122031/d01-x01-y01
Title=Mass spectrum for $\bar{B}\to X_s\gamma$
XLabel=$M_{X_s}$ [GeV]
YLabel=$\text{d}\mathrm{Br}/\text{d}M_X \times10^{-6}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
