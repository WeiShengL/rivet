BEGIN PLOT /BABAR_2018_I1691222/d01-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\eta^\prime$
XLabel=$Q_{1,2}^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\sigma/\text{d}Q_1^2/\text{d}Q_1^2$ [$\text{fb}/\text{GeV}^4$]
XCustomMajorTicks=0.5 $\phantom{}_{2<Q^2_{1,2}<10}$ 1.5 $\phantom{}_{10<Q^2_{1,2}<30}$ 2.5 $\stackrel{10<Q_1^2<30}{\phantom{}_{2<Q^2_2<10}}$ 3.5 $\stackrel{30<Q_1^2<60}{\phantom{}_{2<Q^2_2<30}}$ 4.5 $\phantom{}_{30<Q^2_{1,2}<60}$
END PLOT
