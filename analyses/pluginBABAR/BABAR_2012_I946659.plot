BEGIN PLOT /BABAR_2012_I946659/d01-x01-y0[1,2]
XLabel=$m_{D^0p}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^0p}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d01-x02-y0[1,2]
XLabel=$m_{D^{*0}p}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^{*0}p}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d01-x0[1,2]-y0[3,4]
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{p\bar{p}}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d0[2,3]-x0[1,2,3,4]-y01
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{p\bar{p}}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x0[1,2,3,4]-y04
XLabel=$m_{p\pi^-}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{p\pi^-}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x0[1,2]-y04
XLabel=$m_{p\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{p\pi^\mp}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x0[3,4]-y04
XLabel=$m_{p\pi^-}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{p\pi^-}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d01-x01-y01
Title=Differential Br w.r.t. $D^0p$ mass for $\bar{B}^0\to D^0p\bar{p}$ $(m^2_{p\bar{p}}>5\,\text{GeV}^2)$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d01-x01-y02
Title=Differential Br w.r.t. $D^0p$ mass for $\bar{B}^0\to D^0p\bar{p}$ $(m^2_{p\bar{p}}<5\,\text{GeV}^2)$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d01-x01-y03
Title=Differential Br w.r.t. $p\bar{p}$ mass for $\bar{B}^0\to D^0p\bar{p}$ $(m^2_{D^0p}>9\,\text{GeV}^2)$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d01-x01-y04
Title=Differential Br w.r.t. $p\bar{p}$ mass for $\bar{B}^0\to D^0p\bar{p}$ $(m^2_{D^0p}<9\,\text{GeV}^2)$
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d01-x02-y01
Title=Differential Br w.r.t. $D^{*0}p$ mass for $\bar{B}^0\to D^{*0}p\bar{p}$ $(m^2_{p\bar{p}}>5\,\text{GeV}^2)$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d01-x02-y02
Title=Differential Br w.r.t. $D^{*0}p$ mass for $\bar{B}^0\to D^{*0}p\bar{p}$ $(m^2_{p\bar{p}}<5\,\text{GeV}^2)$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d01-x02-y03
Title=Differential Br w.r.t. $p\bar{p}$ mass for $\bar{B}^0\to D^{*0}p\bar{p}$ $(m^2_{D^{*0}p}>10.5\,\text{GeV}^2)$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d01-x02-y04
Title=Differential Br w.r.t. $p\bar{p}$ mass for $\bar{B}^0\to D^{*0}p\bar{p}$ $(m^2_{D^{*0}p}<10.5\,\text{GeV}^2)$
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d02-x01-y01
Title=Differential Br w.r.t. $p\bar{p}$ mass for $\bar{B}^0\to D^+p\bar{p}\pi^-$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x01-y02
Title=Differential Br w.r.t. $D^+\pbar{p}$ mass for $\bar{B}^0\to D^+p\bar{p}\pi^-$
XLabel=$m_{D^+\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^+\bar{p}}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x01-y03
Title=Differential Br w.r.t. $D^+p$ mass for $\bar{B}^0\to D^+p\bar{p}\pi^-$
XLabel=$m_{D^+p}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^+p}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x01-y04
Title=Differential Br w.r.t. $p\pi^-$ mass for $\bar{B}^0\to D^+p\bar{p}\pi^-$
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d02-x02-y01
Title=Differential Br w.r.t. $p\bar{p}$ mass for $\bar{B}^0\to D^{*+}p\bar{p}\pi^-$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x02-y02
Title=Differential Br w.r.t. $D^{*+}\pbar{p}$ mass for $\bar{B}^0\to D^{*+}p\bar{p}\pi^-$
XLabel=$m_{D^{*+}\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^{*+}\bar{p}}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x02-y03
Title=Differential Br w.r.t. $D^{*+}p$ mass for $\bar{B}^0\to D^{*+}p\bar{p}\pi^-$
XLabel=$m_{D^{*+}p}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^{*+}p}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x02-y04
Title=Differential Br w.r.t. $p\pi^-$ mass for $\bar{B}^0\to D^{*+}p\bar{p}\pi^-$
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d02-x03-y01
Title=Differential Br w.r.t. $p\bar{p}$ mass for $B^-\to D^0p\bar{p}\pi^-$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x03-y02
Title=Differential Br w.r.t. $D^0\pbar{p}$ mass for $B^-\to D^0p\bar{p}\pi^-$
XLabel=$m_{D^0\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^0\bar{p}}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x03-y03
Title=Differential Br w.r.t. $D^0p$ mass for $B^-\to D^0p\bar{p}\pi^-$
XLabel=$m_{D^0p}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^0p}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x03-y04
Title=Differential Br w.r.t. $p\pi^-$ mass for $B^-\to D^0p\bar{p}\pi^-$
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d02-x04-y01
Title=Differential Br w.r.t. $p\bar{p}$ mass for $B^-\to D^{*0}p\bar{p}\pi^-$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x04-y02
Title=Differential Br w.r.t. $D^{*0}\pbar{p}$ mass for $B^-\to D^{*0}p\bar{p}\pi^-$
XLabel=$m_{D^{*0}\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^{*0}\bar{p}}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x04-y03
Title=Differential Br w.r.t. $D^{*0}p$ mass for $B^-\to D^{*0}p\bar{p}\pi^-$
XLabel=$m_{D^{*0}p}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^{*0}p}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d02-x04-y04
Title=Differential Br w.r.t. $p\pi^-$ mass for $B^-\to D^{*0}p\bar{p}\pi^-$
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d03-x01-y01
Title=Differential Br w.r.t. $p\bar{p}$ mass for $\bar{B}^0\to D^0p\bar{p}\pi^-\pi^+$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x01-y02
Title=Differential Br w.r.t. $D^0\pbar{p}$ mass for $\bar{B}^0\to D^0p\bar{p}\pi^-\pi^+$
XLabel=$m_{D^0\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^0\bar{p}}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x01-y03
Title=Differential Br w.r.t. $D^0p$ mass for $\bar{B}^0\to D^0p\bar{p}\pi^-\pi^+$
XLabel=$m_{D^0p}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^0p}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x01-y04
Title=Differential Br w.r.t. $p\pi^\mp$ mass for $\bar{B}^0\to D^0p\bar{p}\pi^-\pi^+$
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d03-x02-y01
Title=Differential Br w.r.t. $p\bar{p}$ mass for $\bar{B}^0\to D^{*0}p\bar{p}\pi^-\pi^+$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x02-y02
Title=Differential Br w.r.t. $D^{*0}\pbar{p}$ mass for $\bar{B}^0\to D^{*0}p\bar{p}\pi^-\pi^+$
XLabel=$m_{D^{*0}\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^{*0}\bar{p}}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x02-y03
Title=Differential Br w.r.t. $D^{*0}p$ mass for $\bar{B}^0\to D^{*0}p\bar{p}\pi^-\pi^+$
XLabel=$m_{D^{*0}p}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^{*0}p}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x02-y04
Title=Differential Br w.r.t. $p\pi^-$ mass for $\bar{B}^0\to D^{*0}p\bar{p}\pi^-\pi^+$
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d03-x03-y01
Title=Differential Br w.r.t. $p\bar{p}$ mass for $B^-\to D^+p\bar{p}\pi^-\pi^-$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x03-y02
Title=Differential Br w.r.t. $D^+\pbar{p}$ mass for $B^-\to D^+p\bar{p}\pi^-\pi^-$
XLabel=$m_{D^+\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^+\bar{p}}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x03-y03
Title=Differential Br w.r.t. $D^+p$ mass for $B^-\to D^+p\bar{p}\pi^-\pi^-$
XLabel=$m_{D^+p}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^+p}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x03-y04
Title=Differential Br w.r.t. $p\pi^-$ mass for $B^-\to D^+p\bar{p}\pi^-\pi^-$
END PLOT

BEGIN PLOT /BABAR_2012_I946659/d03-x04-y01
Title=Differential Br w.r.t. $p\bar{p}$ mass for $B^-\to D^{*+}p\bar{p}\pi^-\pi^-$
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x04-y02
Title=Differential Br w.r.t. $D^{*+}\pbar{p}$ mass for $B^-\to D^{*+}p\bar{p}\pi^-\pi^-$
XLabel=$m_{D^{*+}\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^{*+}\bar{p}}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x04-y03
Title=Differential Br w.r.t. $D^{*+}p$ mass for $B^-\to D^{*+}p\bar{p}\pi^-\pi^-$
XLabel=$m_{D^{*+}p}$ [$\mathrm{GeV}$]
YLabel=$\mathrm{d}\mathcal{B} /\mathrm{d}m_{D^{*+}p}$ [$10^{-5}/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I946659/d03-x04-y04
Title=Differential Br w.r.t. $p\pi^-$ mass for $B^-\to D^{*+}p\bar{p}\pi^-\pi^-$
END PLOT
