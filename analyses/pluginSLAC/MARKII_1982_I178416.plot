BEGIN PLOT /MARKII_1982_I178416/d01-x01-y01
Title=Charged Particle Momentum at 5.2 GeV
XLabel=$x_p$
YLabel=$s/\sigma\mathrm{d}\sigma/\mathrm{d}x_p$ [$\mu\mathrm{b}\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /MARKII_1982_I178416/d01-x01-y02
Title=Charged Particle Momentum at 6.5 GeV
XLabel=$x_p$
YLabel=$s/\sigma\mathrm{d}\sigma/\mathrm{d}x_p$ [$\mu\mathrm{b}\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /MARKII_1982_I178416/d01-x01-y03
Title=Charged Particle Momentum at 29 GeV
XLabel=$x_p$
YLabel=$s/\sigma\mathrm{d}\sigma/\mathrm{d}x_p$ [$\mu\mathrm{b}\mathrm{GeV}^2$]
END PLOT
