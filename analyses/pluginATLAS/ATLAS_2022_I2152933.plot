# BEGIN PLOT /ATLAS_2022_I2152933/*
Title=
YLabel=Fraction of events
XTwosidedTicks=1
YTwosidedTicks=1
LegendXPos=0.9
LegendYPos=0.95
LegendAlign=r
RatioPlotYMin=0.6
RatioPlotYMax=1.4
ErrorBars=1
LogY=0
BottomMargin=1.2
# END PLOT

BEGIN PLOT /ATLAS_2022_I2152933/d07-x01-y01
XLabel=$n_\text{ch}$
YLabel=$1 / \sigma \;\text{d}\sigma / \text{d} n_{\text{ch}}$
YLabelSep=6.5
RatioPlotYMin=0.5
RatioPlotYMax=2.2
YMax=0.035
END PLOT

BEGIN PLOT /ATLAS_2022_I2152933/d08-x01-y01
XLabel=$\sum\limits_{n_\tex{ch}} p_\tex{T}$ [GeV]
YLabel=$1 / \sigma \;\text{d}\sigma / \text{d} \sum\limits_{n_\tex{ch}} p_\tex{T}$ [1/GeV]
YLabelSep=6.5
RatioPlotYMin=0.4
RatioPlotYMax=2.
YMax=0.025
END PLOT

BEGIN PLOT /ATLAS_2022_I2152933/d09-x01-y01
XLabel=$\sum\limits_{n_\tex{ch}} p_T, n_\tex{ch} <20$ [GeV]
YLabel=$1 / \sigma \;\text{d}^2\sigma / \text{d} n_{\text{ch}} \text{d} \sum\limits_{n_\tex{ch}} p_\tex{T}$ [1/GeV]
YLabelSep=6.5
YMax=0.00025
RatioPlotYMax=2.
RatioPlotYMin=0.6
END PLOT

BEGIN PLOT /ATLAS_2022_I2152933/d10-x01-y01
XLabel=$\sum\limits_{n_{ch}} p_T, n_\tex{ch} \in [20, 40)$ [GeV]
YLabel=$1 / \sigma \;\text{d}^2\sigma / \text{d} n_{\text{ch}} \text{d} \sum\limits_{n_\tex{ch}} p_\tex{T}$ [1/GeV]
YLabelSep=6.5
YMax=0.0003
RatioPlotYMax=1.3
RatioPlotYMin=0.2
END PLOT

BEGIN PLOT /ATLAS_2022_I2152933/d11-x01-y01
XLabel=$\sum\limits_{n_{ch}} p_T, n_\tex{ch} \in [40, 60)$ [GeV]
YLabel=$1 / \sigma \;\text{d}^2\sigma / \text{d} n_{\text{ch}} \text{d} \sum\limits_{n_\tex{ch}} p_\tex{T}$ [1/GeV]
YLabelSep=6.5
YMax=0.0003
RatioPlotYMax=1.6
RatioPlotYMin=0.7
END PLOT

BEGIN PLOT /ATLAS_2022_I2152933/d12-x01-y01
XLabel=$\sum\limits_{n_{ch}} p_T, n_\tex{ch} \in [60, 80)$ [GeV]
YLabel=$1 / \sigma \;\text{d}^2\sigma / \text{d} n_{\text{ch}} \text{d} \sum\limits_{n_\tex{ch}} p_\tex{T}$ [1/GeV]
YLabelSep=6.5
YMax=0.0003
RatioPlotYMax=1.7
RatioPlotYMin=0.7
END PLOT

BEGIN PLOT /ATLAS_2022_I2152933/d13-x01-y01
XLabel=$\sum\limits_{n_{ch}} p_T, n_\tex{ch} \geq 80$ [GeV]
YLabel=$1 / \sigma \;\text{d}^2\sigma / \text{d} n_{\text{ch}} \text{d} \sum\limits_{n_\tex{ch}} p_\tex{T}$ [1/GeV]
YLabelSep=6.5
YMax=0.0001
RatioPlotYMax=2.
RatioPlotYMin=0.3
END PLOT

