BEGIN PLOT /ATLAS_2022_I2077570/d01-x01-y01
XLabel=$p_{\mathrm{T},ll}$ [GeV]
YLabel=$d\sigma/dp_{\mathrm{T},ll}$ [pb/GeV]
LogY=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d02-x01-y01
XLabel=$p_{\mathrm{T},j1}$ [GeV]
YLabel=$d\sigma/dp_{\mathrm{T},j1}$ [pb/GeV]
LogY=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d03-x01-y01
XLabel=$N_{jets}$
YLabel=$d\sigma/dN_{jets}$ [pb]
LogY=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d04-x01-y01
Title=$p_{\mathrm{T},j1} > 500$ GeV
XLabel=$N_{jets}$
YLabel=$d\sigma/dN_{jets}$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d05-x01-y01
Title=$p_{\mathrm{T},j1} > 500$ GeV
XLabel=$\Delta R_{Z,j}^{min}$
YLabel=$d\sigma/d\Delta R_{Z,j}^{min}$ [pb]
LegendXPos=0.15
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d06-x01-y01
Title=$p_{\mathrm{T},j1} > 500$ GeV
XLabel=$r_{Z,j}$
YLabel=$d\sigma/dr_{Z,j}$ [pb]
LegendXPos=0.15
YMax = 0.2
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d07-x01-y01
Title=$p_{\mathrm{T},j1} > 500$ GeV $+$ $\Delta R_{Z,j}^{min} < 1.4$
XLabel=$r_{Z,j}$
YLabel=$d\sigma/dr_{Z,j}$ [pb]
LogY=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d08-x01-y01
Title=$p_{\mathrm{T},j1} > 500$ GeV $+$ $\Delta R_{Z,j}^{min} > 2.0$
XLabel=$r_{Z,j}$
YLabel=$d\sigma/dr_{Z,j}$ [pb]
LegendXPos=0.15
LogY=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d09-x01-y01
Title=$p_{\mathrm{T},j1} > 500$ GeV $+$ $\Delta R_{Z,j}^{min} < 1.4$
XLabel=$N_{jets}$
YLabel=$d\sigma/dN_{jets}$ [pb]
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d10-x01-y01
Title=$p_{\mathrm{T},j1} > 500$ GeV $+$ $\Delta R_{Z,j}^{min} > 2.0$
XLabel=$N_{jets}$
YLabel=$d\sigma/dN_{jets}$ [pb]
LogY=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d11-x01-y01
XLabel=$H_{\mathrm{T}}$ [GeV]
YLabel=$d\sigma/dH_{\mathrm{T}}$ [pb / GeV]
LogY=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d12-x01-y01
Title=$S_{\mathrm{T}} > 600$ GeV
XLabel=$\Delta R_{Z,j}^{min}$
YLabel=$d\sigma/d\Delta R_{Z,j}^{min}$ [pb]
YMax=0.4
LogY=1
END PLOT

BEGIN PLOT /ATLAS_2022_I2077570/d13-x01-y01
Title=$S_{\mathrm{T}} > 600$ GeV
XLabel=$N_{jets}$
YLabel=$d\sigma/dN_{jets}$ [pb]
YMax=0.4
LogY=1
END PLOT
