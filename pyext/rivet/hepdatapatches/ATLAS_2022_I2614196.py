import yoda

def patch(path, ao):
    ao.rmAnnotation('ErrorBreakdown')
    needs_patching = [
        '/REF/ATLAS_2022_I2614196/d15-x01-y01',
        '/REF/ATLAS_2022_I2614196/d16-x01-y01',
    ]
    if path in needs_patching:
        tmp = 'REF/ATLAS_2022_I2614196/d%d-x01-y0%d'
        n = 51 if 'd15' in path else 52
        out = [ ]
        for offset in range(5):
            newpath = tmp % (n, offset+1)
            newao = yoda.Scatter2D()
            newao.setPath(newpath)
            for idx in range(0,40,5):
                p = ao.point(idx + offset)
                newao.addPoint(p.x(), p.z(), p.xErrs(), p.zErrs())
            out.append(newao)
        return out
    return ao

